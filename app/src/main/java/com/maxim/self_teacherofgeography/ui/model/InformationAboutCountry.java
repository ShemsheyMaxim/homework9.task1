package com.maxim.self_teacherofgeography.ui.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Максим on 29.05.2017.
 */

public class InformationAboutCountry implements Serializable {

    private String region;
    private String subregion;
    private String name;
    private String capital;
    private int population;
    private List<Language> languages;
    private String borders[];
    private double area;

    public InformationAboutCountry(String region, String subregion, String name, String capital, int population,
                                   List<Language> languages, String[] borders, double area) {
        this.region = region;
        this.subregion = subregion;
        this.name = name;
        this.capital = capital;
        this.population = population;
        this.languages = languages;
        this.borders = borders;
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public String getSubregion() {
        return subregion;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public int getPopulation() {
        return population;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public String[] getBorders() {
        return borders;
    }

    public double getArea() {
        return area;
    }
}

package com.maxim.self_teacherofgeography.ui;

import com.maxim.self_teacherofgeography.ui.Retrofit;
import com.maxim.self_teacherofgeography.ui.model.InformationAboutCountry;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by Максим on 29.05.2017.
 */

public class Retrofit {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/v2/all")
//        http://restcountries.eu/rest/v2/all?fields=region;subregion;name
        void getInformation(Callback<List<InformationAboutCountry>> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getInformation(Callback<List<InformationAboutCountry>> callback) {
        apiInterface.getInformation(callback);
    }
}

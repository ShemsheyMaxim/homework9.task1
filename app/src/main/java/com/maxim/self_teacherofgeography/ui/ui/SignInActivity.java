package com.maxim.self_teacherofgeography.ui.ui;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.maxim.self_teacherofgeography.R;
import com.maxim.self_teacherofgeography.ui.Retrofit;
import com.maxim.self_teacherofgeography.ui.dataBaseHelper.DataBaseHelper;
import com.maxim.self_teacherofgeography.ui.model.InformationAboutCountry;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.maxim.self_teacherofgeography.ui.dataBaseHelper.DataBaseHelper.TABLE_COUNTRIES;
import static com.maxim.self_teacherofgeography.ui.dataBaseHelper.DataBaseHelper.TABLE_REGIONS;
import static com.maxim.self_teacherofgeography.ui.dataBaseHelper.DataBaseHelper.TABLE_SUBREGIONS;


public class SignInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        TextView signUp = (TextView) findViewById(R.id.textView_sign_up);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View signUp) {
                Intent intent = new Intent(SignInActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        final DataBaseHelper dataBaseHelper = new DataBaseHelper(this);

        Retrofit.getInformation(new Callback<List<InformationAboutCountry>>() {

            @Override
            public void success(List<InformationAboutCountry> informationAboutCountries, Response response) {

                SQLiteDatabase writableDatabase = dataBaseHelper.getWritableDatabase();

//                Set<InformationAboutCountry> set = new HashSet<>();
//                set.addAll(informationAboutCountries);
                List<InformationAboutCountry> list = new ArrayList<>();
                list.addAll(informationAboutCountries);
//                List<InformationAboutCountry> list = new ArrayList<>();
//                for (final InformationAboutCountry informationAboutCountry : set) {
//                    list.add(informationAboutCountry);
//                }
                String countRegions = "SELECT count(*) FROM region";
                Cursor cursorRegion = writableDatabase.rawQuery(countRegions, null);
                cursorRegion.moveToFirst();
                int icountRegion = cursorRegion.getInt(0);
                if (icountRegion == 0) {
                    Set<String> treeSetRegion = new TreeSet<>();
                    for (final InformationAboutCountry informationAboutCountry : list) {
                        treeSetRegion.add(informationAboutCountry.getRegion());
                    }
                    ContentValues cvRegion = new ContentValues();
//                    for (int i = treeSetRegion.size() - 1; i >= 0; i--) {
                    for (int i = 0; i < treeSetRegion.size(); i++) {
//                    cvRegion.put("region", list.get(i).getRegion());
                        cvRegion.put("region", (String) treeSetRegion.toArray()[i]);
                        long rowID = writableDatabase.insert(TABLE_REGIONS, null, cvRegion);
                    }
                }

                String countSubregions = "SELECT count(*) FROM subregion";
                Cursor cursorSubregion = writableDatabase.rawQuery(countSubregions, null);
                cursorSubregion.moveToFirst();
                int icountSubregion = cursorSubregion.getInt(0);
                if (icountSubregion == 0) {
                    Set<String> treeSetSubregion = new TreeSet<String>();
                    for (final InformationAboutCountry informationAboutCountry : list) {
                        treeSetSubregion.add(informationAboutCountry.getSubregion());
                    }
                    ContentValues cvSubregion = new ContentValues();
                    for (int i = 0; i < treeSetSubregion.size(); i++) {
                        cvSubregion.put("subregion", (String) treeSetSubregion.toArray()[i]);
                        long rowID = writableDatabase.insert(TABLE_SUBREGIONS, null, cvSubregion);
                    }
                }

                String countCountries = "SELECT count(*) FROM countries";
                Cursor cursorCountry = writableDatabase.rawQuery(countCountries, null);
                cursorCountry.moveToFirst();
                int icountCountry = cursorCountry.getInt(0);
                if (icountCountry == 0) {
//                Set<InformationAboutCountry> treeSetCountryName = new TreeSet<InformationAboutCountry>();
//                for (final InformationAboutCountry informationAboutCountry: list){
//                    treeSetCountryName.addAll(list);
//                }
                    ContentValues cvCountry = new ContentValues();
//                    for (int i = list.size() - 1; i >= 0; i--) {
                    for (int i = 0; i < list.size(); i++) {
                        cvCountry.put("country_name", list.get(i).getName());
                        cvCountry.put("country_capital", list.get(i).getCapital());
                        cvCountry.put("country_population", list.get(i).getPopulation());
                        cvCountry.put("country_area", list.get(i).getArea());
                        long rowID = writableDatabase.insert(TABLE_COUNTRIES, null, cvCountry);
                    }
                }
                writableDatabase.close();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Exception with HTTP!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

package com.maxim.self_teacherofgeography.ui.dataBaseHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Максим on 30.06.2017.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    public static String DB_NAME = "DataBase";
    public static int DB_VERSION = 1;

    public static String TABLE_REGIONS = "region";
    public static final String KEY_ID_REGION = "region_id";
    public static final String KEY_REGION = "region";

    public static String TABLE_SUBREGIONS = "subregion";
    public static final String KEY_ID_SUBREGION = "subregion_id";
    public static final String KEY_SUBREGION = "subregion";
    public static final String KEY_REGION_ID = "region_id";

    public static String TABLE_COUNTRIES = "countries";
    public static final String KEY_ID_COUNTRY = "country_id";
    public static final String KEY_COUNTRY_NAME = "country_name";
    public static final String KEY_COUNTRY_CAPITAL = "country_capital";
    public static final String KEY_COUNTRY_POPULATION = "country_population";
    public static final String KEY_COUNTRY_AREA = "country_area";
    public static final String KEY_SUBREGION_ID = "subregion_id";

    public static String TABLE_USERS = "users";
    public static final String KEY_ID_USER = "user_id";
    public static final String KEY_LOGIN = "login";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_COUNTRY = "country";

    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_REGIONS + "(" + KEY_ID_REGION
                + " integer primary key autoincrement," + KEY_REGION + " text" + ")");

        db.execSQL("create table " + TABLE_SUBREGIONS + "(" + KEY_ID_SUBREGION
                + " integer primary key autoincrement," + KEY_SUBREGION + " text, "
                + KEY_REGION_ID + " integer" + ")");

        db.execSQL("create table " + TABLE_COUNTRIES + "(" + KEY_ID_COUNTRY
                + " integer primary key autoincrement," + KEY_COUNTRY_NAME + " text," + KEY_COUNTRY_CAPITAL + " text,"
                + KEY_COUNTRY_POPULATION + " integer," + KEY_COUNTRY_AREA + " integer,"
                + KEY_SUBREGION_ID + " integer" + ")");

        db.execSQL("create table " + TABLE_USERS + "(" + KEY_ID_USER
                + " integer primary key autoincrement," + KEY_LOGIN + " text," + KEY_PASSWORD + " text,"
                + KEY_COUNTRY + " text" + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_REGIONS);

        db.execSQL("drop table if exists " + TABLE_SUBREGIONS);

        db.execSQL("drop table if exists " + TABLE_COUNTRIES);

        db.execSQL("drop table if exists " + TABLE_USERS);

        onCreate(db);
    }
}

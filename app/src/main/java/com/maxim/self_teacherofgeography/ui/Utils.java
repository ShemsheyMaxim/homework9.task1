package com.maxim.self_teacherofgeography.ui;

import android.os.Environment;

/**
 * Created by Максим on 22.06.2017.
 */

public final class Utils {
    private Utils() {
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
}

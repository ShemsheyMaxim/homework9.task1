package com.maxim.self_teacherofgeography.ui.ui;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.maxim.self_teacherofgeography.R;
import com.maxim.self_teacherofgeography.ui.dataBaseHelper.DataBaseHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    EditText login, password, country;
    TextInputLayout loginLayout, passwordLayout, countryLayout;
    Button register;
    TextView result, back;
    DataBaseHelper dataBaseHelperUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        login = (EditText) findViewById(R.id.editText_login);
        password = (EditText) findViewById(R.id.editText_password);
        country = (EditText) findViewById(R.id.editText_country);


        loginLayout = (TextInputLayout) findViewById(R.id.login_layout);
        passwordLayout = (TextInputLayout) findViewById(R.id.password_layout);
        countryLayout = (TextInputLayout) findViewById(R.id.country_layout);

        login.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (login.getText().toString().isEmpty()) {
                    loginLayout.setErrorEnabled(true);
                    loginLayout.setError(getResources().getString(R.string.login_empty));
                } else {
                    loginLayout.setErrorEnabled(false);
                }
                passwordLayout.setErrorEnabled(false);
                countryLayout.setErrorEnabled(false);
            }
        });

        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (password.getText().toString().isEmpty()) {
                    passwordLayout.setErrorEnabled(true);
                    passwordLayout.setError(getResources().getString(R.string.password_empty));
                } else {
                    passwordLayout.setErrorEnabled(false);
                }
                loginLayout.setErrorEnabled(false);
                countryLayout.setErrorEnabled(false);
            }
        });

        country.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (country.getText().toString().isEmpty()) {
                    countryLayout.setErrorEnabled(true);
                    countryLayout.setError(getResources().getString(R.string.country_empty));
                } else {
                    countryLayout.setErrorEnabled(false);
                }
                loginLayout.setErrorEnabled(false);
                passwordLayout.setErrorEnabled(false);
            }
        });

        result = (TextView) findViewById(R.id.textView_result);

        register = (Button) findViewById(R.id.button_register);

        dataBaseHelperUser = new DataBaseHelper(this);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();

                String login = loginLayout.getEditText().getText().toString();
                String password = passwordLayout.getEditText().getText().toString();
                String country = countryLayout.getEditText().getText().toString();

                SQLiteDatabase database = dataBaseHelperUser.getWritableDatabase();

                ContentValues contentValues = new ContentValues();

                if (!validateLogin(login)) {
                    loginLayout.setError("Not a valid login!");
                } else if (!validatePassword(password)) {
                    passwordLayout.setError("Not a valid password!");
                } else {
                    loginLayout.setErrorEnabled(false);
                    passwordLayout.setErrorEnabled(false);
                    countryLayout.setErrorEnabled(false);

                    contentValues.put(DataBaseHelper.KEY_LOGIN, login);
                    contentValues.put(DataBaseHelper.KEY_PASSWORD, password);
                    contentValues.put(DataBaseHelper.KEY_COUNTRY, country);

                    result.setText("OK! You account registered");

                    database.insert(DataBaseHelper.TABLE_USERS,null,contentValues);
                }
            }
        });
        back = (TextView) findViewById(R.id.textView_back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, SignInActivity.class);
                startActivity(intent);
            }
        });
        dataBaseHelperUser.close();
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public boolean validateLogin(String login) {
        return login.length() > 5;
    }

    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";
    //    private Pattern pattern;
    private Matcher matcher;
    private Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

    public boolean validatePassword(String password) {
        matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
